import React from 'react';
// import logo from './logo.svg';
import './App.css';

import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import Home from "./pages/Home"
import NewTodo from "./pages/NewTodo"
function App () {
  return (
    <Router>
      <Route exact path="/" component={Home} />
      <Route path="/new" component={NewTodo} />
    </Router>
  );
}

export default App;
