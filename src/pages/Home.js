import React from "react";

import { Link } from "react-router-dom";
import { Query } from "react-apollo";
import gql from "graphql-tag"; // installed by apollo boost

const ListTodos = gql`
	query {
		todoes {
			id
			description
			completed
		}
	}
`;

export default function Home() {
	return (
		<Query query={ListTodos}>
			{({ loading, data, error }) => {
				if (loading) {
					return <div>loading</div>;
				}
				return (
					<>
						<Link to="/new">New Todo + </Link>
						<ul>
							{data.todoes.map(todo => (
								<li>{todo.description}</li>
							))}
						</ul>
					</>
				);
			}}
		</Query>
	);
}
