import React, { useState } from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

const AddTodo = gql`
	mutation($description: String!, $completed: Boolean!) {
		createTodo(data: { description: $description, completed: $completed }) {
			description
			completed
		}
	}
`;
export default function NewTodo({ history }) {
	const [todo, setTodo] = useState("");
    
    const handleSubmit = async (e, todo, createTodo) => {
		e.preventDefault();
		console.log("Hey");

		try {
			await createTodo({ variables: { description: todo, completed: false } });
			history.push("/");
		} catch (error) {
			console.log("Error", error);
		}
	};

	return (
		<Mutation mutation={AddTodo}>
			{createTodo => (
				<form onSubmit={e => handleSubmit(e, todo, createTodo)}>
					<input
						type="text"
						name="todo"
						value={todo}
						onChange={e => setTodo(e.target.value)}
					/>
					<button type="submit">Add</button>
				</form>
			)}
		</Mutation>
	);
}
